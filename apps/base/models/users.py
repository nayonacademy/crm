from django.db import models
from django.contrib.auth.models import AbstractBaseUser

ROLE = (
    ('customer', 'Customer'),
    ('provider', 'Provider'),
)


class User(AbstractBaseUser):
    user_role = models.CharField(max_length=100, choices=ROLE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
