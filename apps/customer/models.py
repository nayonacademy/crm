from django.db import models

# Create your models here.
from apps.provider.models import Car


class RentACar(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    booking_date = models.DateTimeField()
    booking_status = models.BooleanField()
