from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Car(models.Model):
    user = models.CharField()
    name = models.CharField()
    car_model = models.CharField()
    verification = models.BooleanField()
    feactured = models.BooleanField()
    topranked = models.IntegerField()
    recent_avaliable = models.BooleanField()
    vehicle_Details = models.CharField()
    status = models.CharField()


class CarReservedTime(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()


class CarDocuments(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    documents = models.FileField()


class Review(models.Model):
    provider = models.ForeignKey(User, on_delete=models.CASCADE)
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    review_rating = models.IntegerField()
    car = models.ForeignKey(Car, on_delete=models.CASCADE)