from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from apps.provider.models import Car


class Payments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)